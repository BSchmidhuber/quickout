<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    include "functions.php";

    $type = getTypeParam();

    if (!isset($_GET['id']))
        redirect("user-overview.php?type=$type&deleted=false");

    ensureUserLogin();

    $id = $_GET['id'];
    $deleted = "false";

    switch ($type)
    {
        case TYPE_WORKOUT:
            // Absicherung, dass normale User nur eigene Workouts löschen können
            if (!isAdminUser() &&
                !getWorkoutFromUser($id, getCurrentUserId()))
                break;

            deleteWorkout($id);
            $deleted = "true";
            break;

        case TYPE_EXERCISE:
            // Absicherung, dass normale User nur eigene Übungen löschen können
            if (!isAdminUser() &&
                !getExerciseFromUser($id, getCurrentUserId()))
                break;

            deletePictureFromExercise($id);
            deleteExercise($id);
            $deleted = "true";
            break;
    }

    $referer = $_SERVER['HTTP_REFERER'];
    if (strpos($referer, '?') !== false)
        $referer = substr($referer, 0, strpos($referer, '?'));
    redirect($referer . "?type=" . $type . "&deleted=" . $deleted);
?>

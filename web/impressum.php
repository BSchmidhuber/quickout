<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    include "functions.php";
    includeHeader("Impressum", "impressum");
?>
<h1>Impressum</h1>
<p><strong>QuickOut - Die Workout App</strong></p>
<h2>Kontakt</h2>
<p><a href="mailto:bschmidhuber.mmt-b2019@fh-salzburg.ac.at">Bernhard Schmidhuber</a></p>
<p>(Programmierung, Inhalt und Bildrechte Übungs-Grafiken)</p>
<h2>Über das Projekt</h2>
<p>MultiMediaProjekt 1 (MMP1)</p>
<p>Fachbereich:
    <br><a href="https://www.multimediatechnology.at" target="_blank">Multimedia Technology (MMT)</a>
    <br><a href="https://www.fh-salzburg.ac.at" target="_blank">FH Salzburg</a>
</p>
<h2>Datenschutz</h2>
<p>Folgende Daten werden beim User-Login in einer Datenbank gespeichert: Name, E-Mail.<br>
    Diese Daten werden nur zur Identifikation beim Login gespeichert und haben keinen weiteren Verwendungszweck.<br>
    Wenn Sie möchten, dass wir diese Daten löschen, ist dies mit der Löschung des Benutzerkontos verbunden.<br>
    Senden Sie zum Löschen des Benutzerkontos einfach eine <a href="mailto:bschmidhuber.mmt-b2019@fh-salzburg.ac.at">
        E-Mail an bschmidhuber.mmt-b2019@fh-salzburg.ac.at</a>.</p>
<?php
    includeFooter();
?>

<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    include "functions.php";

    ensureUserLogin();

    if (!isset($_GET) || !isset($_GET['id']))
        throw new InvalidArgumentException("Übungs-ID nicht definiert!");

    $id = $_GET['id'];

    $type = getTypeParam();

    if (isset($_POST) && isset($_POST['name'])) // Bearbeitung wurde schon gemacht
    {
        require_once 'vendor/autoload.php';

        $purifier = new HTMLPurifier(HTMLPurifier_Config::createDefault());
        $name = $purifier->purify($_POST['name']);

        switch ($type)
        {
            case TYPE_WORKOUT:
                try
                {
                    beginTransaction();

                    $public = isAdminUser() ? $_POST['isPublic']
                        : getValuesFromWorkout($id, "public")->public;

                    // Workout-Name updaten
                    updateExistingWorkout($id, $name, $public);

                    // Exercises löschen und alle neu zuweisen
                    deleteExistingWorkoutsExercises($id);

                    foreach ($_POST['exercises'] as $exercise)
                        insertNewWorkoutExercise($id, $exercise);

                    endTransaction(true);
                }
                catch (Exception $e)
                {
                    endTransaction(false);
                }
                break;
            case TYPE_EXERCISE:
                try
                {
                    beginTransaction();

                    $public = isAdminUser() ? $_POST['isPublic']
                        : getValuesFromExercise($id, "public")->public;

                    if (isset($_FILES['file']) && $_FILES['file']['name'] != "")
                    {
                        // Altes Bild ggf. vom Server löschen
                        deletePictureFromExercise($id);

                        // Neues Bild am Server speichern
                        $picturePath = saveExercisePicture($_FILES['file']);
                    }

                    // Übungs-Name updaten
                    updateExistingExercise($id, $name, $picturePath, $public);

                    endTransaction(true);
                }
                catch (Exception $e)
                {
                    endTransaction(false);
                }
                break;
        }

        redirect("user-overview.php?type=$type");
    }

    $name = "";
    switch ($type)
    {
        case TYPE_WORKOUT:
            $typeNameS = "Workout";
            $name = getValuesFromWorkout($id, "name")->name;
            break;
        case TYPE_EXERCISE:
            $typeNameS = "Übung";
            $name = getValuesFromExercise($id, "name")->name;
            break;
    }

    $name = htmlspecialchars($name);

    $title = "$typeNameS bearbeiten";
    includeHeader($title, "user");

    include "user-form.php";

    includeFooter();
?>

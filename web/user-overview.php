<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    include "functions.php";

    ensureUserLogin();

    $userId = getCurrentUserId();

    includeHeader("Übersicht", "user");

    $type = getTypeParam();
    include "user-overview-content.php";
?>
<script>document.title = "QuickOut - Übersicht " + <?= $typeNameP ?>;</script>
<?php
    includeFooter();
?>

<?php
// Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
// Studiengang Multimedia Technology (MMT), FH Salzburg

include "functions.php";
includeHeader("Startseite", "home no-scroll", true);
?>

<img src="img/logo/schriftzug.png" id="main-logo" alt="QuickOut-Logo">
<img src="img/exercises/upload/1/windmill.svg" id="main-picture" alt="WorkOut Strichmännchen">
<div id="home-buttons">
    <button onclick="window.location='workout.php'">Start</button>
    <?php
    if (isUserLoggedIn()) {
    ?>
        <button onclick="window.location='user.php'">Mein QuickOut</button>
    <?php
    } else {
    ?>
        <button onclick="window.location='login.php'">Login</button>
    <?php
    }
    ?>
    <button id="btnAppInstall">App installieren</button>
    <button id="btnActivatePush" disabled>
        Push Notifications
    </button>
</div>

<?php
includeFooter();
?>
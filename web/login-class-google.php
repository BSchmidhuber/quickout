<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    require_once "login-oauth.php";

    class LoginGoogle implements LoginOAuth
    {
        private $client;
        private $userData = null;

        public function __construct()
        {
            $this->client = new Google_Client();

            require "login-config.php";

            if (!isset($googleClientId) || !isset($googleClientSecret))
                die ('Please create login-config.php, define $googleClientId and $googleClientSecret there. See login-config-sample.php');

            $this->client->setClientId($googleClientId);
            $this->client->setClientSecret($googleClientSecret);

            $this->client->setApplicationName("QuickOut");
            $this->client->setRedirectUri(getRedirectUri("login-google.php"));

            $this->client->addScope("https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile");
        }

        public function getOAuthProvider(): string
        {
            return "Google";
        }

        function getLoginUrl()
        {
            return $this->client->createAuthUrl();
        }

        function getUserData()
        {
            if ($this->userData == null)
            {
                $oAuth = new Google_Service_Oauth2($this->client);
                $this->userData = $oAuth->userinfo_v2_me->get();
            }

            return $this->userData;
        }

        public function getAccessToken()
        {
            return $this->client->fetchAccessTokenWithAuthCode($_GET['code'])['access_token'];
        }

        public function setAccessToken($accessToken)
        {
            $this->client->setAccessToken($accessToken);
        }

        public function getEmail()
        {
            $userData = $this->getUserData();
            return $userData['email'];
        }

        public function getName()
        {
            $userData = $this->getUserData();
            return $userData['name'];
        }
    }

?>
<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    if ($_SERVER['HTTP_HOST'] == 'users.multimediatechnology.at')
    {
        $db_config['DB_NAME'] = "fhs44418_mmp1";
        $db_config['DB_USER'] = "fhs44418";
        $db_config['DB_PASS'] = "";  // fill in password here!!
    }
    else
    {
        $db_config['DB_NAME'] = "mmp1";
        $db_config['DB_USER'] = ""; // fill in your local db-username here!!
        $db_config['DB_PASS'] = ""; // fill in password here!!
    }
?>

// Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
// Studiengang Multimedia Technology (MMT), FH Salzburg

const cacheName = "quickout_v1";

self.addEventListener("install", (event) => {
  console.log("[Service Worker] Install");
});

self.addEventListener("activate", (event) => {
  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.map((cache) => {
          if (cache !== cacheName) {
            console.log("[Service Worker] Clearing Old Cache");
            return caches.delete(cache);
          }
        })
      );
    })
  );
});

self.addEventListener("fetch", (event) => {
  const request = event.request;
  event.respondWith(
    fetch(request)
      .then((response) => {
        const responseClone = response.clone();
        caches.open(cacheName).then((cache) => {
          // Ignore requests where url starts with 'chrome-extension://'
          if (request.url.indexOf("chrome-extension://") === 0) return;

          // Only cache responses with status code 200
          if (response.status !== 200) return;

          cache.put(request, responseClone);
        });
        return response;
      })
      .catch(() => caches.match(request))
  );
});

self.addEventListener("message", (event) => {
  const data = event.data;

  console.log("[Service Worker] Received message:", data);

  event.source.postMessage(
    "Thank you for your message! Yours, Service Worker 😉"
  );

  if (data.status === "finished") {
    console.log(
      `[Service Worker] Workout erfolgreich abgeschlossen! Du hast ${data.count} Übungen erledigt!`
    );
    return;
  }
});

self.addEventListener("push", (event) => {
  console.log(
    `[Service Worker] Push received with data: "${event.data.text()}"`
  );

  const title = "QuickOut - Die schnelle Workout App";
  const options = {
    body: event.data.text(),
    icon: "img/logo/logo.png",
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener("notificationclick", (event) => {
  console.log("[Service Worker] Notification click received.");

  event.preventDefault();
  event.notification.close();

  const urlToOpen = "https://quickout.bschmidhuber.at";
  const promiseChain = clients
    .matchAll({
      type: "window",
      includeUncontrolled: true,
    })
    .then((windowClients) => {
      let matchingClient = null;
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i];
        if (windowClient.url.startsWith(urlToOpen)) {
          matchingClient = windowClient;
          break;
        }
      }

      return matchingClient
        ? matchingClient.focus()
        : clients.openWindow(urlToOpen);
    });
  event.waitUntil(promiseChain);
});

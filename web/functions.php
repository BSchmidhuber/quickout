<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    require_once "functions-db.php";
    require_once "functions-user.php";
    require_once "functions-site.php";

    require_once "login-oauth.php";
    checkUserLoginCookies();

    ini_set('display_errors', true);

    define("TYPE_WORKOUT", "w");
    define("TYPE_EXERCISE", "e");
?>

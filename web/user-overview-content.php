<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    switch ($type)
    {
        case TYPE_WORKOUT:
            $list = getWorkouts($userId, isAdminUser());
            $typeNameS = "Workout";
            $typeNameP = "Workouts";
            break;
        case TYPE_EXERCISE:
            $list = getExercises($userId, isAdminUser());
            $typeNameS = "Übung";
            $typeNameP = "Übungen";
            break;
    }

    unset($deleteMessage);
    if (isset($_GET['deleted']) && $_GET['type'] === $type)
    {
        $deleted = $_GET['deleted'];

        $deleteMessage = ($deleted == "true") ?
            "$typeNameS wurde erfolgreich gelöscht!" :
            "$typeNameS konnte nicht leider gelöscht werden!";
    }
?>
<script>
    function confirmDelete(isWorkout, name)
    {
        if (isWorkout)
            return confirm("Wollen Sie Workout '" + name + "' wirklich und unwiderruflich löschen?");

        return confirm("Wollen Sie Übung '" + name + "' wirklich und unwiderruflich löschen?\n" +
            "Die Übung wird auch aus allen Workouts gelöscht, in denen sie gespeichert war!");
    }
</script>

<h1>Meine <?= $typeNameP ?></h1>

<?php
    if (isset($deleteMessage))
        echo "<p>$deleteMessage</p>";

    if (count($list) === 0)
        echo "<p>Es sind aktuell keine eigenen $typeNameP vorhanden!</p>";
    else
    {
        ?>
        <table>
            <tbody>
                <?php
                    foreach ($list as $element)
                    {
                        ?>
                        <tr <?php
                            if ($element->public === true)
                                echo "class='public'";
                        ?>>
                            <td><?= htmlspecialchars($element->name); ?></td>
                            <?php
                                if ($type == TYPE_EXERCISE)
                                {
                                    echo "<td>";
                                    if ($element->picture !== null)
                                    {
                                        $picture = getPictureServerPath($element->picture);
                                        ?>
                                        <img src="<?= $picture ?>" alt="<?= htmlspecialchars($element->name) ?>">
                                        <?php
                                    }
                                    else
                                    {
                                        echo "<em>Kein Bild</em>";
                                    }
                                    echo "</td>";
                                }
                            ?>
                            <td><a href="user-edit.php?type=<?= $type ?>&id=<?= $element->id; ?>">Bearbeiten</a>
                            </td>
                            <td><a href="user-delete.php?type=<?= $type ?>&id=<?= $element->id; ?>"
                                   onclick="return confirmDelete('<?= $type === TYPE_WORKOUT ?>', '<?= htmlspecialchars($element->name); ?>')">Löschen</a>
                            </td>
                        </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>
        <?php
    }
?>
<button onclick="window.location='user-create.php?type=<?= $type ?>'"><?= $typeNameS ?> hinzufügen</button>

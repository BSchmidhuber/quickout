const applicationServerPublicKey =
  "BOWJJ_88Vl7hwuEPUYWDyy0fBYbYuCJ3bT3Qkt4UKQyAVeYmMhCjPodfbcZzLMgYH1myGacB_uCgimwaLpqSbDc";

function urlB64ToUint8Array(base64String) {
  const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, "+")
    .replace(/_/g, "/");

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

document.addEventListener("DOMContentLoaded", () => {
  let deferredPrompt;
  const btnAppInstall = document.getElementById("btnAppInstall");
  const btnActivatePush = document.getElementById("btnActivatePush");

  const hideInstallButton = () => {
    if (!btnAppInstall) return;
    btnAppInstall.style.display = "none";
  };

  const activateInstallButton = () => {
    if (!btnAppInstall) return;
    btnAppInstall.style.display = "initial";

    btnAppInstall.addEventListener("click", () => {
      if (deferredPrompt !== undefined) {
        deferredPrompt.prompt();

        deferredPrompt.userChoice.then((choiceResult) => {
          console.log("[PWA] " + choiceResult.outcome);

          if (choiceResult.outcome === "dismissed") {
            console.log("[PWA] User cancelled home screen install");
          } else {
            console.log("[PWA] User added to home screen");
          }

          deferredPrompt = null;
        });
      }
    });
  };

  hideInstallButton();

  if ("serviceWorker" in navigator) {
    // register service worker
    navigator.serviceWorker
      .register("serviceworker.js")
      .then((swRegistration) => {
        console.log("[PWA] Service Worker erfolgreich registriert");

        if (btnActivatePush) {
          let isSubscribed;

          const updateBtn = () => {
            if (isSubscribed) {
              btnActivatePush.textContent = "Push Deaktivieren";
            } else {
              btnActivatePush.textContent = "Push Aktivieren";
            }

            btnActivatePush.disabled = false;
          };

          const updateSubscriptionOnServer = (subscription) => {
            console.log(
              "[PWA] Simulate sending the following subscription to the server:",
              subscription
            );
            if (subscription) {
              console.log(
                "[PWA] For testing purposes, copy the following text and paste it on https://web-push-codelab.glitch.me/"
              );
              console.log('%c' + JSON.stringify(subscription), 'background: #fa0;');
            }
          };

          const subscribeUser = () => {
            const applicationServerKey = urlB64ToUint8Array(
              applicationServerPublicKey
            );
            swRegistration.pushManager
              .subscribe({
                userVisibleOnly: true,
                applicationServerKey: applicationServerKey,
              })
              .then((subscription) => {
                console.log("[PWA] User is subscribed.");

                updateSubscriptionOnServer(subscription);

                isSubscribed = true;

                updateBtn();
              })
              .catch((error) => {
                console.error("[PWA] Failed to subscribe the user: ", error);
                updateBtn();
              });
          };

          const unsubscribeUser = () => {
            swRegistration.pushManager
              .getSubscription()
              .then((subscription) => {
                if (subscription) {
                  return subscription.unsubscribe();
                }
              })
              .catch((error) => {
                console.log("[PWA] Error unsubscribing", error);
              })
              .then(() => {
                updateSubscriptionOnServer(null);

                console.log("[PWA] User is unsubscribed.");
                isSubscribed = false;

                updateBtn();
              });
          };

          const handlePushNotifications = () => {
            // check if push is supported
            if ("PushManager" in window) {
              btnActivatePush.addEventListener("click", function () {
                btnActivatePush.disabled = true;
                if (isSubscribed) {
                  unsubscribeUser();
                } else {
                  subscribeUser();
                }
              });

              swRegistration.pushManager
                .getSubscription()
                .then((subscription) => {
                  isSubscribed = !(subscription === null);

                  updateSubscriptionOnServer(subscription);

                  if (Notification.permission === "denied") {
                    btnActivatePush.textContent = "Keine Push Berechtigung";
                    btnActivatePush.disabled = true;
                    updateSubscriptionOnServer(null);
                    return;
                  }

                  updateBtn();
                });
            }
          };

          handlePushNotifications();
        }
      })
      .catch((error) => {
        console.log(`[PWA] Error on registration! ${error.message}`);
      });

    // add service worker message event listener
    navigator.serviceWorker.addEventListener("message", function (event) {
      console.log("[PWA] Client got message:", event.data);
    });

    // add beforeinstallprompt event listener
    window.addEventListener("beforeinstallprompt", function (e) {
      console.log("[PWA] beforeinstallprompt Event fired");
      e.preventDefault();

      deferredPrompt = e;
      activateInstallButton();
      return false;
    });

    // add appinstalled event listener
    window.addEventListener("appinstalled", () => {
      hideInstallButton();
      deferredPrompt = null;
    });
  }
});

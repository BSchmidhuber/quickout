/*
Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
Studiengang Multimedia Technology (MMT), FH Salzburg
*/

DROP TABLE IF EXISTS workouts_exercises;
DROP TABLE IF EXISTS workouts;
DROP TABLE IF EXISTS exercises;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS oauth_providers;

CREATE TABLE oauth_providers
(
    id   SERIAL PRIMARY KEY,
    name CHARACTER VARYING NOT NULL
);

CREATE TABLE users
(
    id             SERIAL PRIMARY KEY,
    email          CHARACTER VARYING,
    name           CHARACTER VARYING,
    oauth_provider INTEGER REFERENCES oauth_providers (id),
    hash           CHARACTER VARYING
);

CREATE TABLE exercises
(
    id      SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users (id) NOT NULL,
    name    CHARACTER VARYING             NOT NULL,
    picture CHARACTER VARYING,
    public  BOOLEAN DEFAULT FALSE
);

CREATE TABLE workouts
(
    id      SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users (id) NOT NULL,
    name    CHARACTER VARYING             NOT NULL,
    public  BOOLEAN
);

CREATE TABLE workouts_exercises
(
    id          SERIAL PRIMARY KEY,
    workout_id  INTEGER NOT NULL REFERENCES workouts (id),
    exercise_id INTEGER NOT NULL REFERENCES exercises (id)
);

\i 'oauth_providers_insert.sql'
\i 'users_insert.sql'
\i 'exercises_insert.sql'
\i 'workouts_insert.sql'
\i 'workouts_exercises_insert.sql'

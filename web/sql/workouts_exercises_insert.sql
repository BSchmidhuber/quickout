/*
Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
Studiengang Multimedia Technology (MMT), FH Salzburg
*/

\set workoutName '\'Warmup\''

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Jumping Jacks'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Windmill'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Inchworms'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Arm Circles'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Single Leg Hops'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Cross Country Run'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Step Touch'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Butt Kickers'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Corkscrew'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Single Leg Circles'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Punches'));

\set workoutName '\'Full Body Workout\''


INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Jumping Jacks'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Push Ups'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Sit Ups'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Squats'));

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
    VALUES ((SELECT id FROM workouts WHERE name = :workoutName),
            (SELECT id FROM exercises WHERE name = 'Low Plank'));

\set workoutName '\'All Exercises\''

INSERT INTO public.workouts_exercises (workout_id, exercise_id)
SELECT (SELECT id as workout_id FROM workouts WHERE name = 'All Exercises'), id as exercise_id
    FROM exercises
    WHERE public = TRUE;

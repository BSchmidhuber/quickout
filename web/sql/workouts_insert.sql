/*
Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
Studiengang Multimedia Technology (MMT), FH Salzburg
*/

INSERT INTO public.workouts (name, user_id, public)
    VALUES ('Warmup', 1, true);
INSERT INTO public.workouts (name, user_id, public)
    VALUES ('Full Body Workout', 1, true);
INSERT INTO public.workouts (name, user_id, public)
    VALUES ('All Exercises', 1, true);

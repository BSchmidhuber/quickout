/*
Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
Studiengang Multimedia Technology (MMT), FH Salzburg
*/

INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Jumping Jacks', 'jumping-jack.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Windmill', 'windmill.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Push Ups', 'pushup.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Sit Ups', 'situp.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Lunges', 'lunge.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Squats', 'squat.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'High Plank', 'high-plank.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Low Plank', 'low-plank.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Side Plank', 'side-plank.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Bridge', 'bridge.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Wall Sit', 'wall-sit.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Inchworms', 'inchworm.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Mountain Climbers', 'mountain-climber.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Calf Raise', 'calf-raise.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Superman', 'superman.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Triceps Dip', 'triceps-dip.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Arm Circles', 'arm-circle.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Russian Twist', 'russian-twist.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Bicycle Crunches', 'bicycle-crunch.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Crunches', 'crunch.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Single Leg Hops', 'single-leg-hop.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Puppet Spin', 'puppet-spin.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Cross Country Run', 'cross-country-run.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Jump Rope', 'jump-rope.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Butt Kickers', 'butt-kicker.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'High Knees', 'high-knee.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Star Jumps', 'star-jump.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Step Touch', 'step-touch.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Bridge Kicks', 'bridge-kick.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Leg Lift', 'leg-lift.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Corkscrew', 'corkscrew.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Single Leg Circles', 'single-leg-circle.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Squat to High Knees', 'squat-to-high-knee.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Scissor Kicks', 'scissor-kick.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Punches', 'punch.svg', true);
INSERT INTO exercises (user_id, name, picture, public)
    VALUES (1, 'Knee Push Ups', 'knee-pushup.svg', true);

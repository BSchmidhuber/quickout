<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    include "functions.php";

    ensureUserLogin();

    $type = getTypeParam();
    if (isset($_POST) && isset($_POST['name']))
    {
        require_once 'vendor/autoload.php';

        $purifier = new HTMLPurifier(HTMLPurifier_Config::createDefault());
        $name = $purifier->purify($_POST['name']);

        $public = $_POST['isPublic'];

        switch ($type)
        {
            case TYPE_WORKOUT:
                $workoutId = insertNewWorkout(getCurrentUserId(), $name, $public);

                foreach ($_POST['exercises'] as $exercise)
                    insertNewWorkoutExercise($workoutId, $exercise);

                break;
            case TYPE_EXERCISE:
                if (isset($_FILES['file']) && $_FILES['file']['name'] != "")
                    $picturePath = saveExercisePicture($_FILES['file']);

                insertNewExercise(getCurrentUserId(), $name, $picturePath, $public);
                break;
        }

        redirect("user-overview.php?type=$type");
    }

    switch ($type)
    {
        case TYPE_WORKOUT:
            $title = "Neues Workout hinzufügen";
            break;
        case TYPE_EXERCISE:
            $title = "Neue Übung hinzufügen";
            break;
    }

    includeHeader($title, "user");

    $name = "";

    include "user-form.php";

    includeFooter();
?>

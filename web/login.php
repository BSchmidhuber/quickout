<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    include "functions.php";

    if (isUserLoggedIn())
        redirect("user.php");

    include_once "login-class-google.php";
    include_once "login-class-facebook.php";

    $google = new LoginGoogle();
    $facebook = new LoginFacebook();

    includeHeader("Login", "login");
?>
<input type="button" value="Login mit Google" onclick="window.location = '<?= $google->getLoginUrl(); ?>';">
<input type="button" value="Login mit Facebook" onclick="window.location = '<?= $facebook->getLoginUrl(); ?>';">

<?php
    includeFooter();
?>

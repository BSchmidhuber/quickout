<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    function getDatabaseHandle()
    {
        static $dbh = null;
        if ($dbh != null)
            return $dbh;

        require "db-config.php";

        if (!isset($db_config))
            die ('Please create db-config.php, define $db_config array with values there. See db-config-sample.php');

        try
        {
            $DSN = "pgsql:dbname=" . $db_config['DB_NAME'] . ";host=localhost";
            $dbh = new PDO($DSN, $db_config['DB_USER'], $db_config['DB_PASS']);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            return $dbh;
        }
        catch (Exception $e)
        {
            die ("Problem connecting to database " . $db_config['DB_NAME'] . " as " . $db_config['DB_USER'] . ": " . $e->getMessage());
        }
    }

    function beginTransaction()
    {
        $dbh = getDatabaseHandle();
        $dbh->beginTransaction();
    }

    function endTransaction($success)
    {
        $dbh = getDatabaseHandle();
        if ($success)
            $dbh->commit();
        else
            $dbh->rollBack();
    }

    function getWorkouts($userId = null, $public = true)
    {
        $dbh = getDatabaseHandle();

        if ($userId == null)
            return $dbh->query("SELECT * FROM workouts WHERE public = true ORDER BY id")->fetchAll();

        $sql = "SELECT * FROM workouts WHERE user_id = ?";

        if ($public == true)
            $sql .= " OR public = true";

        $sql .= " ORDER BY id";

        $sth = $dbh->prepare($sql);
        $sth->execute(array(getCurrentUserId()));

        return $sth->fetchAll();
    }

    function getExercises($userId = null, $public = true)
    {
        $dbh = getDatabaseHandle();

        if ($userId == null)
            return $dbh->query("SELECT * FROM exercises WHERE public = true ORDER BY id")->fetchAll();

        $sql = "SELECT * FROM exercises WHERE user_id = ?";

        if ($public == true)
            $sql .= " OR public = true";

        $sql .= " ORDER BY id";

        $sth = $dbh->prepare($sql);
        $sth->execute(array(getCurrentUserId()));

        return $sth->fetchAll();
    }

    function getExercisesByWorkout($workoutId)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT * FROM exercises WHERE id IN (SELECT exercise_id FROM workouts_exercises WHERE workout_id = ?)");
        $sth->execute(array($workoutId));
        return $sth->fetchAll();
    }

    function getExerciseIdsByWorkout($workoutId)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT exercise_id FROM workouts_exercises WHERE workout_id = ?");
        $sth->execute(array($workoutId));
        return $sth->fetchAll();
    }

    function getUserDb($userId)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT * FROM users WHERE id = ?");
        $sth->execute(array($userId));
        return $sth->fetch();
    }

    function getUserIdDb($email, $oAuthProvider)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT id FROM users WHERE email = ? AND oauth_provider = 
                                            (SELECT id FROM oauth_providers WHERE name = ?)");
        $sth->execute(array($email, $oAuthProvider));
        return $sth->fetch();
    }

    function getValuesFromWorkout($id, $values)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT $values FROM workouts WHERE id = ?");
        $sth->execute(array($id));
        return $sth->fetch();
    }

    function getValuesFromExercise($id, $values)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT $values FROM exercises WHERE id = ?");
        $sth->execute(array($id));
        return $sth->fetch();
    }

    function countPictureOnExercises($picture)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT count(*) FROM exercises WHERE picture = ?");
        $sth->execute(array($picture));
        return $sth->fetch()->count;
    }

    function getWorkoutFromUser($workoutId, $userId)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT name FROM workouts WHERE id = ? AND user_id = ?");
        $sth->execute(array($workoutId, $userId));
        return $sth->fetch();
    }

    function getExerciseFromUser($exerciseId, $userId)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT name FROM exercises WHERE id = ? AND user_id = ?");
        $sth->execute(array($exerciseId, $userId));
        return $sth->fetch();
    }

    function getOAuthProviderId($name)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT id FROM oauth_providers WHERE name = ?");
        $sth->execute(array($name));
        return $sth->fetch()->id;
    }

    function getOAuthProvider($id)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT name FROM oauth_providers WHERE id = ?");
        $sth->execute(array($id));
        return $sth->fetch()->name;
    }

    function checkDbUserHash($userId, $hash)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("SELECT id FROM users WHERE id = ? AND hash = ?");
        $sth->execute(array($userId, $hash));
        return $sth->fetch() ? true : false;
    }

    function getOrInsertUserDb($email, $name, $oAuthProvider, $hash)
    {
        $user = getUserIdDb($email, $oAuthProvider);
        if ($user)
            return $user->id;

        return insertNewUser($email, $name, getOAuthProviderId($oAuthProvider), $hash);
    }

    function insertNewUser($email, $name, $oauthProvider, $hash)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("INSERT INTO users (email, name, oauth_provider, hash)
            VALUES (?, ?, ?, ?)");
        $sth->execute(array($email, $name, $oauthProvider, $hash));
        return $dbh->lastInsertId();
    }

    function insertNewWorkout($userId, $name, $public)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("INSERT INTO workouts (user_id, name, public) VALUES (?, ?, ?)");
        $sth->execute(array($userId, $name, $public ? "TRUE" : "FALSE"));
        return $dbh->lastInsertId();
    }

    function updateExistingWorkout($workoutId, $name, $public)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("UPDATE workouts SET name = ?, public = ? WHERE id = ?");
        $sth->execute(array($name, $public ? "TRUE" : "FALSE", $workoutId));
    }

    function updateExistingExercise($exerciseId, $name, $picturePath, $public)
    {
        $dbh = getDatabaseHandle();

        $sqlAddOn = "";
        if ($picturePath != null)
            $sqlAddOn = ", picture = '" . $picturePath . "'";

        $sth = $dbh->prepare("UPDATE exercises SET name = ?, public = ? " . $sqlAddOn . " WHERE id = ?");
        $array = array($name, $public ? "TRUE" : "FALSE", $exerciseId);
        $sth->execute($array);
    }

    function insertNewExercise($userId, $name, $picturePath, $public)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("INSERT INTO exercises (user_id, name, picture, public) VALUES (?, ?, ?, ?)");
        $sth->execute(array($userId, $name, $picturePath, $public ? "TRUE" : "FALSE"));
        return $dbh->lastInsertId();
    }

    function insertNewWorkoutExercise($workoutId, $exerciseId)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("INSERT INTO workouts_exercises (workout_id, exercise_id) VALUES (?, ?)");
        $sth->execute(array($workoutId, $exerciseId));
        return $dbh->lastInsertId();
    }

    function deleteExistingWorkoutsExercises($workoutId)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("DELETE FROM workouts_exercises WHERE workout_id = ?");
        $sth->execute(array($workoutId));
    }

    function deleteWorkout($workoutId)
    {
        deleteExistingWorkoutsExercises($workoutId);

        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("DELETE FROM workouts WHERE id = ?");
        $sth->execute(array($workoutId));
    }

    function deleteExercise($exerciseId)
    {
        $dbh = getDatabaseHandle();
        $sth = $dbh->prepare("DELETE FROM workouts_exercises WHERE exercise_id = ?");
        $sth->execute(array($exerciseId));

        $sth = $dbh->prepare("DELETE FROM exercises WHERE id = ?");
        $sth->execute(array($exerciseId));
    }

?>
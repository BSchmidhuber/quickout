<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    include "functions.php";

    ensureUserLogin();

    $userId = getCurrentUserId();

    $user = getUserDb($userId);

    includeHeader("Mein QuickOut", "user");
?>
<h1>Mein QuickOut</h1>
<table>
    <tbody>
        <tr>
            <td>Name:</td>
            <td><?= $user->name; ?></td>
        </tr>
        <tr>
            <td>E-Mail:</td>
            <td><?= $user->email; ?></td>
        </tr>
        <tr>
            <td>Login-System:</td>
            <td><?= getOAuthProvider($user->oauth_provider); ?></td>
        </tr>
        <?php
            if (isAdminUser())
            {
                ?>
                <tr class="public">
                    <td>Rechte:</td>
                    <td>Administrator</td>
                </tr>
                <?php
            }
        ?>
    </tbody>
</table>
<?php
    $type = TYPE_WORKOUT;
    include "user-overview-content.php";

    $type = TYPE_EXERCISE;
    include "user-overview-content.php";
?>
<input type="button" onclick="window.location = 'logout.php';" value="Logout">
<?php
    includeFooter();
?>

<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    use Facebook\Facebook;
    use Facebook\Helpers\FacebookRedirectLoginHelper;

    require_once "login-oauth.php";

    class LoginFacebook implements LoginOAuth
    {
        private $client;
        private $accessToken = null;
        private $userData = null;

        public function __construct()
        {
            require "login-config.php";

            if (!isset($facebookAppId) || !isset($facebookAppSecret))
                die ('Please create login-config.php, define $googleClientId and $googleClientSecret there. See login-config-sample.php');

            $this->client = new Facebook([
                'app_id' => $facebookAppId,
                'app_secret' => $facebookAppSecret,
                'default_graph_version' => 'v2.10'
            ]);
        }

        public function getOAuthProvider(): string
        {
            return "Facebook";
        }

        function getHelper(): FacebookRedirectLoginHelper
        {
            return $this->client->getRedirectLoginHelper();
        }

        function getLoginUrl()
        {
            return $this->getHelper()->getLoginUrl(getRedirectUri("login-facebook.php"), array('email'));
        }

        function getUserData()
        {
            if ($this->userData == null)
                $this->userData = $this->client->get('/me?locale=de_DE&fields=name,email', $this->getAccessToken())->getGraphUser();

            return $this->userData;
        }

        public function getAccessToken(): string
        {
            if ($this->accessToken == null)
                $this->accessToken = $this->getHelper()->getAccessToken();

            return (string)$this->accessToken;
        }

        public function setAccessToken($accessToken)
        {
            $this->client->setDefaultAccessToken($accessToken);
        }

        public function getEmail()
        {
            return $this->getUserData()->getEmail();
        }

        public function getName()
        {
            return $this->getUserData()->getName();
        }
    }

?>
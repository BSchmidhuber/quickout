// Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
// Studiengang Multimedia Technology (MMT), FH Salzburg

const durationExercise = 30;
const durationPreparation = 10;
const pauseInterval = 5; // längere Pause alle ... Übungen

function CountException(message) {
  this.message = message;
  this.name = "CountException";
}

function showPage(pageId) {
  document.getElementById("workout-select").style.display = "none";
  document.getElementById("workout-start").style.display = "none";
  document.getElementById("workout-finish").style.display = "none";

  document.getElementById(pageId).style.display = "flex";
}

function start() {
  saveSelectionSettings();

  let fetchUrl =
    "getWorkoutExercises.php?id=" +
    document.getElementById("workoutId").value +
    "&random=" +
    document.getElementById("random").value;

  fetch(fetchUrl)
    .then((response) => response.json())
    .then((exercises) => {
      if (exercises.length <= 0)
        throw new CountException("Workout enthält keine Übungen");

      showPage("workout-start");

      startWorkout(
        exercises,
        parseInt(document.getElementById("exerciseCount").value)
      );
    })
    .catch((exception) => {
      alert(exception.message);
    });
}

function saveSelectionSettings() {
  localStorage.setItem("workoutId", document.getElementById("workoutId").value);
  localStorage.setItem("random", document.getElementById("random").checked);
  localStorage.setItem(
    "exerciseCount",
    document.getElementById("exerciseCount").value
  );
}

function loadSelectionSettings() {
  if (localStorage.getItem("workoutId") !== null)
    document.getElementById("workoutId").value =
      localStorage.getItem("workoutId");

  document.getElementById("random").checked = !(
    localStorage.getItem("random") === "false"
  );

  let exerciseCount = localStorage.getItem("exerciseCount") || 20;
  document.getElementById("exerciseCount").value = exerciseCount;
  updateDurationValue(exerciseCount);
}

function updateDurationValue(value) {
  const exercises = parseInt(value);
  const minutes = Math.round(calcWorkoutDuration(value) / 60);

  const exercisesString = exercises === 1 ? "Übung" : "Übungen";
  const minutesString = minutes === 1 ? "Minute" : "Minuten";

  document.getElementById(
    "duration-value"
  ).innerHTML = `${exercises} ${exercisesString} / ~ ${minutes} ${minutesString}`;
}

function calcWorkoutDuration(countExercises) {
  let countPauses = Math.floor((countExercises - 1) / pauseInterval);

  return (
    countExercises * (durationPreparation + durationExercise) +
    countPauses * durationExercise
  );
}

let workoutPaused = false;

function triggerWorkoutPaused() {
  workoutPaused = !workoutPaused;
}

async function startWorkout(exercisesJson, countExercises) {
  const pauseExercise = {
    name: "Pause",
    picture: "pause.svg",
    user_id: null,
  };

  function calculateExercises() {
    let exercises = [];

    let exerciseCounter = 0;
    let pauseCounter = 0;
    while (exercises.length < countExercises + pauseCounter) {
      // Solange noch nicht genug Übungen vorhanden sind, immer wieder von Vorne alle Exercises hinzufügen
      for (let exercise of exercisesJson) {
        exercises.push(exercise);
        exerciseCounter++;

        if (exercises.length === countExercises + pauseCounter) {
          // Abbrechen, wenn genug Exercises hinzugefügt wurden
          break;
        }

        if (exerciseCounter % pauseInterval === 0) {
          exercises.push(pauseExercise);
          pauseCounter++;
        }
      }
    }

    document.getElementById("progress_workout_range").max =
      calcWorkoutDuration(countExercises);

    return exercises;
  }

  let exercisesTotal = calculateExercises();

  let workoutSeconds = 0;

  // Bildschirm aktiv lassen
  let noSleep = new NoSleep();
  noSleep.enable();

  for (let exercise of exercisesTotal) {
    if (exercise.name !== "Pause") await runExercise(exercise, true); // Vorbereitung auf nächste Übung

    await runExercise(exercise, false);
  }

  noSleep.disable();

  if ("serviceWorker" in navigator) {
    const payload = {
      status: "finished",
      count: exercisesTotal.length,
    };

    console.log(`[Workout] Send Notification: ${JSON.stringify(payload)}`);
    navigator.serviceWorker.controller.postMessage(payload);
  }

  showPage("workout-finish");
  return;

  async function runExercise(exercise, isPreparation) {
    // Dies ist so notwendig, damit der Timer funktioniert!
    return await new Promise((resolve) => {
      let exercisePreTitle = "";
      let duration = durationExercise;
      let boxColor = "#EEEEEE";
      let sound = "notify-finish";

      if (isPreparation) {
        exercisePreTitle = "Vorbereitung auf ";
        duration = durationPreparation;
        boxColor = "#CCCCCC";
        sound = "notify-begin";
      }

      function styleBoxes() {
        // Wenn Hochformat, nur Exercise-BG-Color setzen
        if (window.matchMedia("(orientation: portrait").matches) {
          document.getElementById("exercise").style.backgroundColor = boxColor;
          return;
        }

        // Wenn Querformat, 2 Boxen anzeigen:

        // Exercise-Element auf übergeordneten Hintergrund setzen (fix hell)
        let exerciseElement = document.getElementById("exercise");
        exerciseElement.style.backgroundColor =
          exerciseElement.parentElement.style.backgroundColor;

        // Alle Child-Elemente als Box anzeigen (Hintergrund ggf. dunkler)
        for (let element of document.getElementById("exercise").children)
          element.style.backgroundColor = boxColor;
      }

      styleBoxes();

      let imageSource = "img/exercises/";
      if (exercise.name === "Pause") imageSource += exercise.picture;
      else if (exercise.picture === null) imageSource += "default.svg";
      else imageSource += "upload/" + exercise.user_id + "/" + exercise.picture;

      document.getElementById("exercise_title").innerHTML =
        exercisePreTitle + exercise.name;
      document.getElementById("exercise_picture").src = imageSource;
      document.getElementById("exercise_picture").alt =
        exercisePreTitle + exercise.name;

      document.getElementById("progress_exercise_seconds_passed").innerText =
        "00:01";
      document.getElementById("progress_exercise_seconds_total").innerText =
        "00:" + printTwoDigits(duration);
      document.getElementById("progress_exercise_range").max = duration;

      let exerciseSeconds = 0;

      const exerciseTimer = setInterval(runExerciseTimer, 1000);

      function runExerciseTimer() {
        if (workoutPaused) return;

        document.getElementById("progress_workout_range").value =
          ++workoutSeconds + 1;
        document.getElementById("progress_exercise_range").value =
          ++exerciseSeconds + 1;

        let maxSeconds = document.getElementById("progress_workout_range").max;
        document.getElementById("progress_workout_percent").innerText = (
          ((workoutSeconds + 1) * 100) /
          maxSeconds
        ).toFixed(0);
        if (exerciseSeconds === duration) {
          resolve(true);
          document.getElementById("progress_exercise_range").value = 1;
          clearInterval(exerciseTimer);
          return;
        }

        // 1 Sekunde vor Ende Sound abspielen (dauert länger)
        if (exerciseSeconds === duration - 1)
          document.getElementById(sound).play();

        document.getElementById("progress_exercise_seconds_passed").innerText =
          "00:" + printTwoDigits(exerciseSeconds + 1);
      }
    });
  }

  function printTwoDigits(n) {
    return (n < 10 ? "0" : "") + n;
  }
}

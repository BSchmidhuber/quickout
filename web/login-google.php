<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    include "functions.php";
    startSession();

    require "login-class-google.php";

    loginUser(new LoginGoogle());

    redirect("user.php");
?>
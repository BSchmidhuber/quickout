<?php
// Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
// Studiengang Multimedia Technology (MMT), FH Salzburg

include "functions.php";
includeHeader("Workout", "workout no-scroll");
?>
<script src="workout.js"></script>

<div id="workout-select">
    <h1>Workout auswählen</h1>
    <div class="custom-select">
        <select title="Workout-Selection" name="workoutId" id="workoutId">
            <?php
$workouts = getWorkouts(getCurrentUserId());
foreach ($workouts as $workout) {
    $name = htmlspecialchars($workout->name);
    echo "<option value='$workout->id'>$name</option>";
}
?>
        </select>
    </div>

    <div id="random_toggle">
        <label for="random" class="medium">Zufällige Reihenfolge</label>
        <label class="custom-switch">
            <input type="checkbox" name="random" id="random" checked>
            <span class="slider round"></span>
        </label>
    </div>

    <h1>Dauer festlegen</h1>
    <label for="exerciseCount" hidden="hidden">Anzahl der Übungen</label>
    <div class="custom-range">
        <input type="range" class="range" id="exerciseCount" min="1" max="100" name="exerciseCount"
            oninput="updateDurationValue(this.value)">
    </div>
    <div id="duration-value-label" class="medium">
        <span id="duration-value"></span>
    </div>

    <input type="button" value="Workout starten" onclick="start()">
    <script>
    loadSelectionSettings();
    </script>
</div>

<div id="workout-start">
    <script src="libraries/NoSleep.js/NoSleep.min.js"></script>

    <audio id="notify-begin" src="sounds/notify.wav"></audio>
    <audio id="notify-finish" src="sounds/notify-3.wav"></audio>

    <div id="exercise">
        <div>
            <h1 id="exercise_title">Übungsname</h1>
            <img id="exercise_picture" src="#" alt="Bild zur Übung" onclick="triggerWorkoutPaused()">
        </div>

        <div class="big">
            <div class="black hidePortrait">Fortschritt:</div>
            <div class="blue">
                <span id="progress_exercise_seconds_passed" class="verybig"></span> /
                <span id="progress_exercise_seconds_total"></span>
            </div>
            <div class="custom-range">
                <label for="progress_exercise_range" hidden="hidden">Dauer aktuelle Übung</label>
                <input class="bluerange" type="range" id="progress_exercise_range" min="1" value="0" disabled>
            </div>
        </div>
    </div>
    <div id="progress_workout">
        <label for="progress_workout_range" hidden="hidden">Dauer gesamtes Workout</label>
        <div class="custom-range">
            <input class="range" type="range" id="progress_workout_range" min="1" value="0" disabled>
        </div>
        <div id="progress_workout_label" class="big hidePortrait">Gesamtfortschritt <span
                id="progress_workout_percent">0</span>%
        </div>
    </div>

</div>

<div id="workout-finish">
    <h1>Workout erfolgreich abgeschlossen!</h1>
    <input type="button" value="Workout wiederholen" onclick="start()">
    <input type="button" value="Zurück zur Auswahl" onclick="showPage('workout-select')">
</div>

<?php
includeFooter();
?>
<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    require_once "vendor/autoload.php"; // external dependency included via composer: update via Tools -> Composer -> Update
    require_once "functions-user.php";

    interface LoginOAuth
    {
        public function getOAuthProvider();

        public function getLoginUrl();

        public function getAccessToken();

        public function setAccessToken($accessToken);

        public function getEmail();

        public function getName();
    }

    function checkUserLoginCookies()
    {
        startSession();
        if (!areAllCookiesSet())
            return false;

        $userId = $_COOKIE['user_id'];
        $hash = $_COOKIE['hash'];

        if (!checkDbUserHash($userId, $hash))
        {
            logoutUser();
            return false;
        }

        setLoginCookies($_COOKIE['access_token'], $userId, $_COOKIE['email'], $_COOKIE['name'], $hash);
        return true;
    }

    function loginUser(LoginOAuth $loginOAuth)
    {
        if (checkUserLoginCookies())
            return;

        if (isset($_SESSION['access_token']))
        {
            $accessToken = $_SESSION['access_token'];
            $loginOAuth->setAccessToken($accessToken);
        }
        else if (isset($_GET['code']))
        {
            $accessToken = $loginOAuth->getAccessToken();
        }
        else
        {
            redirect("login.php");
            return;
        }

        $email = $loginOAuth->getEmail();
        $name = $loginOAuth->getName();
        $hash = hash("md5", $email . $name);

        $userId = getOrInsertUserDb($email, $name, $loginOAuth->getOAuthProvider(), $hash);

        startSession(true);
        setLoginCookies($accessToken, $userId, $email, $name, $hash);
    }

    function getRedirectUri($targetFile)
    {
        $http = "https://";
        if ($_SERVER['HTTP_HOST'] == 'localhost')
            $http = "http://";

        return str_replace("login.php", $targetFile,
            $http . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
    }

?>
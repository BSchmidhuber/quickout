<?php
// Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
// Studiengang Multimedia Technology (MMT), FH Salzburg

function startSession($regenerate = false)
{
    if (session_status() == PHP_SESSION_NONE) 
        session_start();

    if ($regenerate === true)
    {
        session_regenerate_id();
        session_destroy();
        unset($_SESSION);
        session_start();
    }
}

function isUserLoggedIn()
{
    startSession();
    return isset($_SESSION['user_id']);
}

function ensureUserLogin()
{
    if (!isUserLoggedIn())
        redirect("login.php");
}

function getCurrentUserId()
{
    if (!isUserLoggedIn())
        return null;

    return $_SESSION['user_id'];
}

function isAdminUser()
{
    require "login-config.php";
    if (!isset($adminEmail))
        return false;

    return ($_SESSION['email'] === $adminEmail);
}

function setLoginCookie($cookieName, $value)
{
    $_SESSION[$cookieName] = $value;
    setcookie($cookieName, $value, time() + (60 * 60 * 24 * 365));
}

function deleteLoginCookie($cookieName)
{
    unset($_SESSION[$cookieName]);
    setcookie($cookieName, "", time() - 1);
}

function areAllCookiesSet()
{
    if (isset($_COOKIE['access_token']) &&
        isset($_COOKIE['user_id']) &&
        isset($_COOKIE['email']) &&
        isset($_COOKIE['name']) &&
        isset($_COOKIE['hash']))
        return true;

    return false;
}

function setLoginCookies($accessToken, $userId, $email, $name, $hash)
{
    setLoginCookie("access_token", $accessToken);
    setLoginCookie("user_id", $userId);
    setLoginCookie("email", $email);
    setLoginCookie("name", $name);
    setLoginCookie("hash", $hash);
}

function logoutUser()
{
    startSession(true);
    deleteLoginCookie("access_token");
    deleteLoginCookie("user_id");
    deleteLoginCookie("email");
    deleteLoginCookie("name");
    deleteLoginCookie("hash");
    session_destroy();
}

function getRelativePicturePath($picture, $userId = null)
{
    if ($userId === null)
        $userId = getCurrentUserId();

    return "/img/exercises/upload/" . $userId . "/" . $picture;
}

function getPictureServerPath($picture, $userId = null)
{
    // Relative Server Path (for showing in html)
    $serverDirectory = dirname($_SERVER['PHP_SELF']);

    if ($serverDirectory == "/")
        $serverDirectory = "";

    return $serverDirectory . getRelativePicturePath($picture, $userId);
}

function getPictureFilePath($picture = "")
{
    // Absolute OS-Filepath (for file operations)
    if ($_SERVER['SERVER_NAME'] === "users.multimediatechnology.at")
        return $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/mmp1" . getRelativePicturePath($picture);

    return $_SERVER['DOCUMENT_ROOT'] . getPictureServerPath($picture);
}

function saveExercisePicture($file)
{
    $directoryName = getPictureFilePath();
    $filename = basename($file['name']);

    if (!is_dir($directoryName))
        mkdir($directoryName);

    $tmpFileName = $file['tmp_name'];

    if ($tmpFileName == "" || !move_uploaded_file($tmpFileName, $directoryName . $filename))
    {
        echo("Fehler beim Datei-Upload!");
        return null;
    }

    return $filename;
}

function deletePictureFromExercise($id)
{
    $values = getValuesFromExercise($id, "picture");

    if ($values->picture == null || countPictureOnExercises($values->picture) > 1)
        return;

    $picturePath = getPictureFilePath($values->picture);
    if (file_exists($picturePath))
        unlink($picturePath);
}

?>
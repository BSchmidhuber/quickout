<?php
// Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
// Studiengang Multimedia Technology (MMT), FH Salzburg

function redirect($path)
{
    header("Location: $path");
    exit();
}

function getTypeParam()
{
    if (!isset($_GET['type']))
        throw new InvalidArgumentException("Type nicht definiert!");

    $type = $_GET['type'];
    if ($type != TYPE_WORKOUT && $type != TYPE_EXERCISE)
        throw new InvalidArgumentException("Type falsch gesetzt!");

    return $type;
}

function includeHeader($pagetitle, $mainClass, $hideHeaderOnMobile = false)
{
?>
    <!-- Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    Studiengang Multimedia Technology (MMT), FH Salzburg -->
    <!DOCTYPE html>
    <html lang="de">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="QuickOut - Quick Workout App">
        <meta name="keywords" content="Quickout, Workout">
        <meta name="author" content="Bernhard Schmidhuber">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="theme-color" content="#ffaa00">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php
        if ($_SERVER['HTTP_HOST'] != 'localhost') {
            // CSP blockiert auf localhost die Möglichkeit, mit Live Server zu arbeiten (automatisches aktualisieren der Seite während Entwicklung)
        ?>
            <meta http-equiv="Content-Security-Policy" content="default-src 'self' https: 'unsafe-inline'; media-src 'self' data:;">
        <?php
        }
        ?>
        <title>QuickOut - <?= $pagetitle ?></title>
        <link rel="stylesheet" href="style.css">
        <link rel="apple-touch-icon" sizes="180x180" href="img/logo/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="img/logo/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="img/logo/favicon-16x16.png">
        <link rel="manifest" href="manifest.webmanifest">
    </head>

    <body>
        <script src="pwa.js"></script>

        <header <?php
                if ($hideHeaderOnMobile)
                    echo 'class="hideMobile"';
                ?>>
            <a href="index.php"><img src="img/logo/schriftzug-negativ.svg" alt="Logo" id="header-logo"></a>
            <nav>
                <!-- Versteckte Checkbox, die speichert, ob Hamburger-Menü ausgeklappt ist,
            nur in mobile Version notwendig. Checkbox ist IMMER ausgeblendet! -->
                <input type="checkbox" id="toggle">
                <!-- Alles innerhalb des Labels aktiviert/deaktiviert die Checkbox. -->
                <label class="hamburger" for="toggle">
                    <!--        ≡-->
                    <span></span>
                    <span></span>
                    <span></span>
                </label>
                <ul class="menu-items">
                    <li><a href="workout.php">Start</a></li>
                    <?php
                    if (isUserLoggedIn()) {
                    ?>
                        <li class="dropdown"><a href="user.php">Mein QuickOut</a>
                            <ul class="dropdown-content">
                                <li><a href="user-overview.php?type=w">Meine Workouts</a></li>
                                <li><a href="user-overview.php?type=e">Meine Übungen</a></li>
                                <li><a href="user-show-exercises.php">Alle Übungen</a></li>
                                <li><a href="logout.php">Logout</a></li>
                            </ul>
                        </li>
                    <?php
                    } else {
                    ?>
                        <li><a href="login.php">Login</a></li>
                    <?php
                    }
                    ?>
                    <li><a href="impressum.php">Impressum</a></li>
                </ul>
            </nav>
        </header>
        <main class="<?= $mainClass; ?>">
        <?php
    }

    function includeFooter()
    {
        ?>
        </main>
    </body>

    </html>

<?php
    }
?>
<?php
include "functions.php";

if (!isset($_GET['id']) || !isset($_GET['random'])) {
    throw ErrorException("Wrong Parameters");
}

$exercises = getExercisesByWorkout($_GET['id']);

if (isset($_GET['random']) && $_GET['random'] === "on") {
    shuffle($exercises);
}

echo json_encode($exercises);
?>
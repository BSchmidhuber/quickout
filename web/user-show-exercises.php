<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg

    include "functions.php";

    ensureUserLogin();

    includeHeader("Alle Übungen", "user");

?>
<h1>Alle Übungen</h1>
<table>
    <tbody>
        <?php
            foreach (getExercises(getCurrentUserId()) as $element)
            {
                ?>
                <tr <?php
                    if ($element->public === true)
                        echo "class='public'";
                ?>>
                    <td><?= htmlspecialchars($element->name); ?></td>
                    <?php
                        echo "<td>";
                        if ($element->picture !== null)
                        {
                            $picture = getPictureServerPath($element->picture, $element->user_id);
                            ?>
                            <a href="<?= $picture ?>" target="_blank">
                                <img src="<?= $picture ?>" alt="<?= htmlspecialchars($element->name) ?>">
                            </a>
                            <?php
                        }
                        else
                        {
                            echo "<em>Kein Bild</em>";
                        }
                        echo "</td>";
                    ?>
                    <td><em><?= $element->public === true ? "Öffentlich" : "Privat"; ?></em></td>
                </tr>
                <?php
            }
        ?>
    </tbody>
</table>
<?php
    includeFooter();
?>

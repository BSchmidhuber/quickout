<?php
    // Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
    // Studiengang Multimedia Technology (MMT), FH Salzburg
?>
<h1><?= $title ?></h1>
<form action="#" method="post" enctype="multipart/form-data">
    <input type="text" name="name" placeholder="Name eingeben" title="Name"
           value="<?= $name; ?>" required>
    <?php
        if ($type == TYPE_WORKOUT)
        {
            if (isAdminUser())
            {
                $isPublic = isset($id) ? getValuesFromWorkout($id, "public")->public : true;
                ?>
                <p>
                    <label for="isPublic"><input type="checkbox" id="isPublic"
                                                 name="isPublic" <?= $isPublic ? "checked" : "" ?>>Öffentlich</label>
                </p>
                <?php
            }

            $userId = getCurrentUserId();
            $exercises = getExercises($userId);

            if (isset($id))
                $selectedExercises = getExerciseIdsByWorkout($id);
            ?>
            <h2><label for="exercise-list">Übungen auswählen</label></h2>
            <select id="exercise-list" name="exercises[]" multiple required>
                <?php
                    foreach ($exercises as $exercise)
                    {
                        $selected = "";
                        if (isset($selectedExercises))
                        {
                            foreach ($selectedExercises as $selectedExercise)
                            {
                                if ($selectedExercise->exercise_id == $exercise->id)
                                    $selected = "selected";
                            }
                        }
                        ?>
                        <option value='<?= $exercise->id ?>' <?= $selected ?>>
                            <?= htmlspecialchars($exercise->name) ?></option>
                        <?php
                    }
                ?>
            </select>
            <?php
        }
        else
        {
            ?>
            <?php
            if (isset($id))
            {
                $values = getValuesFromExercise($id, "picture, public");
                if ($values != null)
                {
                    $isPublic = $values->public;
                    if (isAdminUser())
                    {
                        ?>
                        <p>
                            <label for="isPublic"><input type="checkbox" id="isPublic"
                                                         name="isPublic" <?= $isPublic ? "checked" : "" ?>>Öffentlich</label>
                        </p>
                        <?php
                    }
                    ?>
                    <img src="<?= getPictureServerPath($values->picture); ?>"
                         alt="<?= $values->picture; ?>">
                    <?php
                }
            }
            ?>
            <label for="file-upload" class="custom-file-upload">Bild auswählen</label>
            <input type="hidden" name="MAX_FILE_SIZE" value="<?= 10 * 1000 * 1000 ?>"/> <!-- Max. Filesize in Bytes -->
            <input type="file" id="file-upload" name="file" accept="image/*">
            <?php
        }
    ?>

    <input type="submit" value="Speichern">
</form>

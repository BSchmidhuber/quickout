# QuickOut - Workout App
> **Konzept** zum Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
### Executive Summary
Mein Projekt **QuickOut** ist eine Web Application, die das schnelle und einfache Durchführen von Workout-Übungen ermöglicht.

Nach Auswahl des gewünschten Workouts und der Dauer erfolgt der Ablauf des Workouts über einen Intervall-Timer, der jeweils eine gewisse Zeitspanne abläuft. Die Übungen werden durch kleinere Vorbereitunsphasen und etwas längere Erholungspausen (nach einer gewissen Anzahl von Übungen) unterbrochen.

Workouts und dazugehörige Übungen sind teilweise von der App vorgegeben, können aber von einem angemeldeten Benutzer auch individuell erstellt und verwendet werden.

### Ausgangssituation und Ziele
Da es bisher keine Workout App geschafft hat, mich persönlich vollständig zu überzeugen, dachte ich mich, ich könnte eine eigene Workout-App nach meinen Vorstellungen machen.

Die **QuickOut**-App soll sich von anderen Workout-Apps hauptsächlich in 2 Punkten abheben:

1. Schneller und unkomplizierter "Quick-Start" eines Workouts, ohne notwendige Benutzer-Anmeldung oder lästiges Eingeben von persönlichen Daten wie Gewicht, Größe, etc.
2. Einfaches Erstellen und Bearbeiten eigener Workouts und Übungen für eine individuellere Nutzung.

Dadurch soll sie für Menschen der Zielgruppe interessant werden und sich von anderen Konkurrenz-Apps abheben. 

### Zielgruppe
Die Hauptzielgruppe sind Menschen, die bereits Erfahrung mit Workouts und Fitness-Übungen haben und auch relativ genaue Vorstellungen davon haben, welche Übungen sie wann machen wollen.

Ebenso soll die App Menschen ansprechen, die bei ihrem Workout-Programm nicht von einer speziellen App bevormundet werden wollen, welche ihnen einen Trainingsplan vorgibt oder sagt, wann und wie oft sie trainieren sollen, sondern selbst die Kontrolle über ihre Workouts behalten wollen.

### Konkurrenzanalyse
Es gibt bereits einige Workout-Apps am Markt, welche eine ähnliche Funktionalität anbieten, wie z.B.

* *Adidas Training*
* *Freeletics Training*
* *Cardio von Fitify*
* und viele andere...

Diese haben allerdings alle eine eingeschränkte Funktionalität in der Free-Version, verpflichtende Benutzeranmeldung oder ein Benachrichtigungssystem, welches häufig darauf hinweist, dass man doch wieder einmal trainieren soll, was durchaus als nervig aufgefasst werden kann.

In diesen Punkten soll sich **QuickOut** davon unterscheiden.

### Projekt-Team
Das Projekt ist ein Einzelprojekt von Bernhard Schmidhuber, weitere Personen sind nicht beteiligt.

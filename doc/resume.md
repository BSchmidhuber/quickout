# QuickOut - Workout App
> **Résumé** zum Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber

In der Umsetzungsphase meines MMP1 habe ich nicht nur vieles dazugelernt, sondern auch bestehendes Wissen - vor allem in **HTML und CSS** - noch einmal wiederholen, vertiefen und erweitern können.

In Sachen **PHP** ging der Lernfortschritt deutlich weiter, da wir in den bisherigen Lehrveranstaltungen nur die Basics dazu gelernt hatten, in der praktischen Anwendung dann vieles aber erst so richtig klar wird, wie und warum man etwas so macht, wie man es macht.

Einen großen Schritt nach Vorne habe ich auch in **JavaScript** gemacht, da sich meine bisherigen Kenntnisse in der Programmiersprache auf das Notwendigste beschränkt haben. Gerade durch den Einbau der Timer-Funktion habe ich diese Sprache dann im Projekt weiter kennengelernt. 

Ganz neu für mich waren die Erfahrungen mit einer **Progressive Web Application** (siehe *technische Dokumentation*), wo ich durch viel Eigenrecherche nicht nur gelernt habe, wie man das umsetzt, sondern auch die Vorteile gegenüber nativen Apps in der heutigen Webentwicklung kennengelernt habe.

Auch mit den **OAuth-basierten Login-Systemen** von Google und Facebook habe ich mich auf Neuland begeben. Durch das Umsetzen von 2 verschiedenen Varianten habe ich auch die Unterschiede gut gesehen und dann auch zum ersten Mal versucht, in **PHP mit Objektorientierung** zu arbeiten und 2 Login-Klassen mit gemeinsamem Interface zu erstellen, was schlussendlich ganz gut funktioniert hat.

Ebenfalls dabei habe ich die Vorzüge des Abhängigkeits-Managers **Composer** kennengelernt, womit man verwendete Bibliotheken sehr einfach und schnell installieren und updaten kann.

Schlussendlich habe ich neben diesen ganzen Punkten aber auch **viele allgemeine Kleinigkeiten** dazugelernt, wie zum Beispiel die Wichtigkeit des Aufbaus einer guten Dateistruktur, das Arbeiten mit der Entwicklungsumgebung PHPStorm, automatisiertes und schnelles Übernehmen von Datenbank-Änderungen, persönliches Zeitmanagement, sich nicht allzu lange mit Kleinigkeiten aufzuhalten, usw...

In Summe freue ich mich sehr, dass ich in diesen wenigen Wochen schon so viel dazulernen konnte.
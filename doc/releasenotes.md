# Release Notes
### Änderungen nach Code Review

#### Neue Funktionen
- Ein laufendes Workout kann mit einem Klick auf das Bild pausiert und wieder fortgesetzt werden
- Admin-Bereich zum Verwalten von öffentlichen Workouts und Übungen hinzugefügt
- Im User-Bereich können alle vorhandenen Übungen (öffentlich und eigene) mit dazugehörigen Bildern angezeigt werden

#### Usability
- "Start" statt "Quickstart" als Bezeichnung
- Redirect beim Löschen und Bearbeiten von Workouts/Übungen auf richtige (vorhergehende) Seite

#### Sicherheit
- Session-Fixation-Attacke verhindert mit Session Regeneration
- XSS-Attacken abgesichert durch HTML Purifier, htmlspecialchars und Content Security Policy

#### Fehlerbehebungen
- Richtiger Singular/Plural in Auswahl-Anzeige
- Buttons und Slider auf iPhones schauen richtig aus
- Fehler mit access_token bei Google Login behoben
- Facebook-Login funktioniert jetzt für jeden Facebook-User (Datenschutz-Erklärung in Impressum hinzugefügt)

###### Interne Fehlerbehebungen und Optimierungen
- Secret API Keys aus Git entfernt
- PWA-Manifest repariert (Kommentare aus Datei entfernt)
- Vereinfachte Logik zum Verspeichern von Übungs-Bildern
- Verwendung von Datenbank-Transaktionen beim Bearbeiten (Performance-Optimierung)
- Allgemeines Code-Refactoring (z.B. Verwendung von PHP-Defines statt Zeichen, TODOs überarbeitet, unnötigen Code entfernt, etc.)
# QuickOut - Workout App
> **Technische Dokumentation** zum Multimediaprojekt 1 (MMP1) von Bernhard Schmidhuber
### Technische Besonderheiten
##### Progressive Web App
Ein großer Teil der Entwicklung war die Umsetzung des Projektes als *Progressive Web App* (PWA). Die PWA besteht aus einem Web-Manifest und einem Service-Worker, der sich um das Caching bzw. die Netzwerkverbindung kümmert.

Im Web-Manifest werden die grundsätzlichen Parameter der Application festgelegt, wie Seitentitel, Hauptfarbe, etc. Diese Daten braucht das Betriebssystem, um die PWA so darzustellen wie eine native App.

Der Service-Worker verwaltet die Netzwerk-Verbindung zwischen Website und Benutzer. Er registriert die Seite im Browser als PWA und legt z.B. fest, ob die Seite über das Netzwerk oder über den Cache geladen werden soll. Wenn die Seite über die Netzwerk-Verbindung geladen wird, wird sie vom Browser im Cache zwischengespeichert, um im Offline-Fall darauf zugreifen zu können. 

##### OAuth-Login-Systeme
Das Projekt verwendet 2 verschiedene OAuth-Login-Systeme bzw. Anbieter:
* Google
* Facebook

Der Zugriff funktioniert jeweils über *PHP-APIs*, welche mit dem Paketmanager *Composer* installiert wurden (siehe **Verwendete Libraries**). Das Projekt muss dabei beim jeweiligen OAuth-Anbieter registriert werden. Über die API kann dann beim Anbieter ein Login-Link angefordert werden, welcher den Benutzer auf die Login-Seite des Anbieters weiterleitet. Durch eine *Redirect-URL* wird der Benutzer nach erfolgreicher Authentifizierung wieder zurück zur Website geleitet, wo dessen Daten weiterverarbeitet werden können (Datenbank-Eintrag, etc.)

### Verwendete Libraries
##### Google Client API und Facebook Graph SDK
* Verwendet für Benutzeranmeldung
* Installation / Update via composer

##### NoSleep.js
* Verwendet, damit Bildschirm während dem Workout aktiv bleibt
* Direkte Einbindung der js-Library (ohne Package-Manager)

### Composer - Dependency Manager
*Composer* ist ein Abhängigkeitsmanager für PHP, mit dem verschiedenste PHP-Libraries eingebunden und verwaltet werden können. Über die Kommandozeile können diese Pakete installiert bzw. auf dem aktuellen Stand gehalten werden. In der Datei *composer.json* werden die zu verwendenden Pakete definiert, den Rest erledigt Composer selbst: es legt einen Ordner *vendor/* an, in dem sich alle notwendigen Dateien für die abhängigen Bibliotheken befinden. 

### Datenbank
##### Datenbank Management System
Als DMS wurde PostgreSQL verwendet.
##### Tabellen
* **users**: enthält die Benutzer inkl. E-Mail und Name und den dazugehörigen OAuth-Anbieter
* **oauth_providers**: enthält alle möglichen OAuth-Anbieter
* **exercises**: enthält alle Übungen, Bilder-Links, einen public-Merker und ggf. eine User-ID
* **workouts**: enthält alle Workouts, einen public-Merker und ggf. eine User-ID
* **workouts_exercises**: enthält die Verbindungen von allen Übungen, die in Workouts enthalten sind

### Code-Struktur
##### Hauptverzeichnis
###### PHP-Dateien
* Startseite (_index.php_)
* Workout-Bereich (_workout*.php_)
* Login und Logout (_login*.php_, _logout.php_)
* Benutzer-Bereich (_user*.php_)
* Impressum (_impressum.php_)
* Spezielle Funktionalitäten (_functions*.php_, _db-config*.php_)
###### JS-Dateien
* Workout-Time (_workout.js_)
* Service-Worker für PWA (_serviceworker.js_)
###### Spezielle Dateien
* Composer-Dateien (_composer.*_)
* Web-Manifest für PWA (_manifest.webmanifest_)
##### *css/*
* Haupt-Stylesheet *style.css*
* Allgemein wichtige Stylesheets (*font.css*, *header.css*)
* Stylesheets für einzelne Seiten / Bereiche in *pages/*
* Stylesheets für Formular-Elemente etc. in *custom/*
##### *fonts/*
* 3 verwendete Design-Schriftarten (*.woff)
  * BubblegumSans (Schriftzug)
  * LilitaOne (Überschriften)
  * ABeeZee (Text)
##### *img/*
* Alle Exercise-Bilder, aufgeteilt in public und upload
* Alle Logo- und Schriftzug-Dateien
##### *libraries/*
* NoSleep.js Library
##### *sounds/*
* Verwendete Sounds für Workout-Timer
##### *sql/*
* *create_db.sql* zum Anlegen der Datenbank-Tabellen
* **_insert.sql* zum Befüllen der Tabellen
##### *vendor/*
* Verzeichnis für abhängige PHP-Libraries
* Automatisch angelegt von Composer
* Nicht in git
